<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\ParserModel;
use yii\web\UploadedFile;


class ParserController extends Controller
{
	/**
	 * Загрузка файла
	 *
	 * @return string
	 */
	public function actionUpload()
	{
		$model = new ParserModel();
		if (Yii::$app->request->isPost) {
			$model->htmlFile = UploadedFile::getInstance($model, 'htmlFile');
			$file = $model->loadFile();
			if (!empty($file)) {
				// file is uploaded successfully
				$arr = $model->parseFile();
				echo json_encode($arr,true);
				die();
			}
		}
		return "";
	}
}