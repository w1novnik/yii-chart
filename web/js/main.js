$('#parser-form').on('beforeSubmit', function () {
    var $yiiform = $(this);
    var yiiformid = $(this).attr("id");
    var button = $('.upload-file');
    var buttonText = button.text();
    button.text("Загрузка...");
    button.attr("disabled","disabled");
    formdata = new FormData(document.getElementById(yiiformid));
    console.log(formdata);
    // отправляем данные на сервер
    $.ajax({
        url: $yiiform.attr('action'),
        data: formdata,
        enctype: 'multipart/form-data',
        processData : false,
        contentType : false,
        type: 'POST',
        success: function ( data ) {
            button.text(buttonText);
            button.removeAttr("disabled");
            var seriesProfit = [];
            var seriesDates = [];
            var items = [];
            var data = JSON.parse(data);
            //заполняем массивы для осей x и y
            data.forEach(function(item, i, data) {
                seriesProfit.push(parseFloat(item.value.toFixed(2)));
                seriesDates.push(item.dateTime);
                items[item.dateTime] = item;
            });

            Highcharts.chart('container', {
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    title: {
                        text: 'Date'
                    },
                    minRange: 5,
                    categories: seriesDates,
                    labels: { enabled:false },
                    scrollbar: {
                        enabled: true
                    },
                },
                yAxis: {
                    title: {
                        text: 'Balance'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 0
                    }
                },
                tooltip: {
                    //форматируем тултипы для графика
                    formatter: function () {
                        return this.points.reduce(function (s, point) {
                            var str = s + "<br/>Ticket:" + items[point.x].ticket+"<br/>";
                            str += "Type:" + items[point.x].type+"<br/>";
                            str += items[point.x].description+"<br/>";
                            str += '<br/>' + point.series.name + ': ' +
                                point.y;
                            return str;
                        }, '<b>' + this.x + '</b>');
                    },
                    shared: true
                },
                series: [{
                    name: 'Balance',
                    data: seriesProfit
                }],
                chart: {
                    zoomType: 'x'
                },
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 1000
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });
        },
        error: function(data){
            button.text(buttonText);
            button.removeAttr("disabled");
            alert("Файл невозможно прочитать");
        }
    });

    return false; // отменяем отправку данных формы
})