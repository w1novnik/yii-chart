<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<?php
use yii\widgets\ActiveForm;
?>
<div class="site-index">
	<?php

    $form = ActiveForm::begin(['action' => '/parser/upload/', 'options' => ['data' => ['pjax' => true], 'id' =>'parser-form', 'enctype' => 'multipart/form-data']]) ?>

	<?= $form->field($model, 'htmlFile')->fileInput()->label('Загрузите файл в формате html') ?>

    <button class="btn btn-primary upload-file">Загрузить</button>

	<?php
    ActiveForm::end();
	?>
    <br><br>
    <div id="container"></div>
</div>
