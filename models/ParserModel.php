<?php

namespace app\models;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

use yii\base\Model;
use yii\web\UploadedFile;
use darkdrim\simplehtmldom\SimpleHTMLDom as SHD;

/**
 * ParserModel модель для обработки загружаемого файла для построения графика.
 *
 *
 */
class ParserModel extends Model
{
	/**
	 * @var UploadedFile
	 */
	public $htmlFile;
	/**
	 * @var String
	 */
	public $path;
	/**
	 * @var array
	 */
	public $parsedArray;

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			// файл должен быть html
			[['htmlFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'html'],
		];
	}

	/**
	 * Загрузка файла
	 * @return string возвращает путь загруженного файла
	 */
	public function loadFile()
	{
		if ($this->validate()) {
			$this->htmlFile->saveAs('uploads/' . $this->htmlFile->baseName . '.' . $this->htmlFile->extension);
			$this->path = 'uploads/' . $this->htmlFile->baseName . '.' . $this->htmlFile->extension;
			return $this->path;
		}
		return '';
	}

	/**
	 * Считывание файла и создание массива для графика
	 * @return array массив для построения графика
	 */
	public function parseFile(){
		$types = ['buy', 'sell', 'stop', 'balance', 'buy stop'];
		$descriptions = ['Size', 'Item', 'Price', 'S / L', 'T / P', 'Close Time', 'Price', 'Commission', 'Taxes', 'Swap'];
		$arr = [];
		$value = 0;
		$html_source = SHD::str_get_html(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/".$this->path));
		foreach($html_source->find('tr') as $element){
			$item = [];
			$item['description'] = "";
			foreach($element->find('td') as $key=>$td){
				
				if($key == 0){
					$item['ticket'] = $td->plaintext;
				}
				else if($key == count($element->find('td')) - 1){
					$item['profit'] = $td->plaintext;
				}
				else if($key == 1){
					$item['dateTime'] = $td->plaintext;
				}
				else if($key == 2 && in_array($td->plaintext,$types)){
					$item['type'] = $td->plaintext;
				}
				else if($key > 2){
					$description = $descriptions[$key - 3];

					$item['description'] .= $description.": ".$td->plaintext."<br>";
				}
			}
			if(!empty($item['type'])){
				$item['description'] .= "Profit: ".$item['profit']."<br>";
				$value = $value + (float)str_replace(' ','',$item['profit']);
				$item['value'] = $value;
				$arr[] = $item;
			}
		}
		return $arr;
	}
}
